# build phase
FROM node:alpine AS builder
WORKDIR '/app'
COPY package.json .
RUN npm install
COPY . .
RUN npm run build

#/app/build <-- all the stuff we care about


# run phase
FROM nginx

# copy something from that other phase we were working on
COPY --from=builder /app/build /usr/share/nginx/html

# default nginx image will start up nginx auto